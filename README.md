# L2 - Java Project

## Description du projet

Création d'un framework de jeu de cartes en réseau.

Jeux visés à intégrer :
- Bataille classique
- Jeu de Poker
- ...

Ce projet est à récupérer via un fork classique depuis Gitlab.

Pour l'utilisation de **Git**, via l'IDE, voici un tutoriel proposé : [Git tutorial](https://florian-lepretre.herokuapp.com/teaching/projetweb/tutogit).

Pour l'utilisation de **Git** en ligne de commande, un manuel vous est également disponible : [Git helper](GIT_HELP.md).

## Remise des travaux

Lien permettant aux étudiants de remettre leurs travaux pour chaque rendu de TP : [GDrive](https://docs.google.com/spreadsheets/d/1kyBPBW--By7wFJwH-nTLJrV_YEK9DkLKd5fXKpduQ7o/edit?usp=sharing)

## Planning

4 semaines soit 4 rendus de TP :
- 29 Mars => 2 Avril : 9h TP
- 5 Avril => 9 Avril : 6h TP
- 12 Avril => 16 Avril : 6h TP
- 19 Avril => 23 Avril : 6h TP

## Liens vers les sujets 

- TP1 : [Card Game](https://jeromebuisine.fr/sources/teaching/2020-2021/L2/JavaProject/L2-JavaProject-tp1.pdf)
- TP2 : [Poker Game](https://jeromebuisine.fr/sources/teaching/2020-2021/L2/JavaProject/L2-JavaProject-tp2.pdf)

## Liens vers les solutions 

- TP initial : [Initial](https://gitlab.com/jbuisine/l2_projet_java) 
- TP1 : [Card Game](https://gitlab.com/jbuisine/l2_projet_java/-/tree/TP1) 

## Autres liens utiles

- [EDT FAC](https://edt.univ-littoral.fr/direct/index.jsp?data=6b052c86649c89d6314052e0c2e2410d6e0bb6b122f08b2bd96b081fc1a619e28edcb5b52c18281858d9e3fb46f326fdf9355e402da57010a9a3c2453eebfdfdb3a09e7f1c48b6784ac7528f840cddad)