package ulco.cardGame.common.games;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public abstract class BordGame implements Game {
    protected String name; //permet de stocker un nom associé au jeu ;
    protected Integer maxPlayers; //permet de savoir le nombre maximum attendu de joueurs pour le jeu
    protected List<Player> players ;// stocke la liste des joueurs. Le type Player, fait référence à l’interface
                                    //ulco.cardGame.common.interfaces.Player disponible
    protected boolean endGame;//garde un état du jeu afin de savoir s’il est fini ou non
    protected boolean started;//permet de stocker un état du jeu afin de savoir s’il peut commencer ou non
    public BordGame (String name, Integer maxPlayers, String filename) throws FileNotFoundException {
        this.name=name;
        this.maxPlayers=maxPlayers;
        this.players=new ArrayList<>();
        this.initialize(filename);
    }
    public abstract void initialize(String filename) throws FileNotFoundException;
    public boolean addPlayer(Player player){
        if(players.size()==maxPlayers || players.contains(player)){
            return false;
        }
        else{
            players.add(player);
            return true;
        }
    }
    public void removePlayer(Player player){
        players.remove(player);
    }
    public void removePlayers(){
        players.removeAll(players);
    }
    public void displayState(){
        for(Player player: players){
            System.out.println("nom :" + player.getName() + "score : " + player.getScore() );
        }
    }
    public boolean isStarted(){
        if(started){
            return true;
        }
        else {
            return false;
        }
    }
    public Integer maxNumberOfPlayers(){
        return maxPlayers;
    }
    public List<Player> getPlayers(){
        return players;
    }

}
