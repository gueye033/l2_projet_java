package ulco.cardGame.common.games;

import ulco.cardGame.common.interfaces.Player;

public abstract class BordPlayer implements Player{
    protected String name;//le pseudo associé au joueur;
    protected boolean playing; //un statut du joueur permettant de savoir s’il joue (associé à un jeu), peutjouer, ou ne peut plus jouer
    protected Integer score; //: score associé au joueur (ce score sera calculé en fonction du jeu visé).



    public  BordPlayer(String name){
        this.name=name;
        this.score=0;
        this.playing=false;
    }
    public void canPlay(boolean playing){
        this.playing=playing;
    }
    public  String getName(){
        return this.name;
    }
    public boolean isPlaying(){
        return this.playing;
    }
    @Override
    public String toString() {
        return "BordPlayer{" +
                "name='" + name + '\'' +
                ", score=" + score +
                '}';
    }
}
