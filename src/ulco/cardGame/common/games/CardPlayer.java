package ulco.cardGame.common.games;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CardPlayer extends BordPlayer{
    private List<Card> cards; //qui permettra de stocker les cartes en main du joueur

    public CardPlayer(String name) {
        super(name);
        this.cards = new ArrayList<>();
    }
    public Integer getScore(){
        return this.score;
    }

    public void addComponent(Component component){
        if(this.cards.size()>52){
            throw new RuntimeException("tu ne peux pas add plus de 52 cards");
        }
        this.cards.add((Card) component);
        this.getScore();

    }
    public void removeComponent(Component component){
        this.cards.remove(component);
        this.getScore();
    }
    @Override
    public Card play(){
            Card removedCard;
            removedCard=cards.get(0);
            removeComponent(removedCard);
            return removedCard;

    }

    public List<Card> getComponents(){
        return cards;
    }
    public void shuffleHand(){
        Collections.shuffle(cards);
    }
    public void clearHand(){
            this.cards.clear();
    }

    @Override
    public String toString() {
        return "CardPlayer{" +
                "name='" + name + '\'' +
                ", score=" + score +
                '}';
    }
}
