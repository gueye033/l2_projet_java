package ulco.cardGame.common.games;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.interfaces.Player;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;


public class PokerGame extends BoardGame{
    private List<Card> cards;
    private List<Coin> coins;
    private Integer numberOfRounds;
    private Integer maxRounds;
    /**
     * Enable constructor of Game
     * - Name of the game
     * - Maximum number of players of the Game
     * - Filename with all required information to load the Game
     *
     * @param name
     * @param maxPlayers
     * @param filename
     */
    public PokerGame(String name, Integer maxPlayers, String filename) {
        super(name, maxPlayers, filename);
    }

    @Override
    public void initialize(String filename) {
        this.cards = new ArrayList<>();
        this.coins = new ArrayList<>();
        this.numberOfRounds = 0;
        this.maxRounds=0;

        // Here initialize the list of Cards
        try {
            File pokerFile = new File(filename);
            Scanner myReader = new Scanner(pokerFile);

            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                String[] dataValues = data.split(";");

                // get Card value
                Integer value = Integer.valueOf(dataValues[1]);
                this.cards.add(new Card(dataValues[0], value,true));
                this.coins.add(new Coin(dataValues[0],value, true));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }


    @Override
    public void run() {
        Player gameWinner = null;
        Collections.shuffle(cards);
        int playerIndex = 0;

        for (Coin coin : coins) {

            players.get(playerIndex).addComponent(coin);
            coin.setPlayer(players.get(playerIndex));

            playerIndex++;

            if (playerIndex >= players.size()) {
                playerIndex = 0;
            }
        }
        while (!this.end()) {
            Map<Player, Coin> playedCard = new HashMap<>();

            for (Player player : players) {

                if (!player.isPlaying())
                    continue;

                // Get card played by current player
                Coin coin= (Coin) player.play();

                // Keep knowledge of card played
                playedCard.put(player, coin);

                System.out.println(player.getName() + " has played " + coin.getName());
            }
        }
        return ;
    }

    @Override
    public boolean end() {
        // check if it's the end of the game
        endGame = true;
        for (Player player : players) {
            if (player.isPlaying()) {
                endGame = false;
            }
        }

        return endGame;
    }
}
