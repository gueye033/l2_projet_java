package ulco.cardGame.common.games.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.List;

public class CardBoard implements Board {
    List<Card> cards;

    public CardBoard(List<Card> cards) {
        this.cards = new ArrayList<>();
    }

    @Override
    public void clean() {
        this.cards.clear();
    }

    @Override
    public void addComponent(Component component) {
        List<Card> Component=new ArrayList<>();
        if(Component instanceof Card){
            cards.add((Card) Component);
        }
    }

    @Override
    public List<Component> getComponents() {
        return new ArrayList<>(cards);
    }

    public List<Component> getSpecificComponent(Class classType) {
        List<Component> components=new ArrayList<>(cards);
        return components;
    }

    @Override
    public void displayState() {
        for(Card carte: cards){
            System.out.println("Voici l'ensemble des carte :" +carte);
        }
    }

}
