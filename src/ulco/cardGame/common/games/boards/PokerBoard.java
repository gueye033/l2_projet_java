
package ulco.cardGame.common.games.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.List;

public class PokerBoard implements Board {
    List<Card> cards;
    List<Coin> coins;
    public PokerBoard(List<Card> cards, List<Coin> coins) {
        this.cards = new ArrayList<>();
        this.coins = new ArrayList<>();
    }

    @Override
    public void clean() {
        this.cards.clear();
        this.coins.clear();
    }

    @Override
    public void addComponent(Component component) {
        // On teste le type du `component` passé en paramètre
        // Si c'est une carte, on ajoute dans la liste des cartes
        if (component instanceof Card)
            cards.add((Card)component);
        // Si c'est un jeton, on ajoute dans la liste des jetons
        // Et on augmente le score du joueur
        if (component instanceof Coin) {
            coins.add((Coin) component);
        }
    }

    @Override
    public List<Component> getComponents() {
        List<Component> tab = new ArrayList<>();
        tab.addAll(cards);
        tab.addAll(coins);
        return tab;

    }


    public List<Component> getSpecificComponent(Class classType) {
        List<Component> components=new ArrayList<>();
        components.addAll(cards);
        components.addAll(coins);
            return components;
    }

    public void displayState() {
        for(Card carte: cards){
            System.out.println("cards=" +carte);
        }
        for(Coin coin: coins){
            System.out.println(", coins=" +coin);
        }
    }
}