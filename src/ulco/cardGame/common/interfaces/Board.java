package ulco.cardGame.common.interfaces;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;

import java.util.List;

public interface Board {
    void clean();
    /**
     * Add new component linked to Board
     */
    void addComponent(Component component);
    /**
     * Get all component in Board
     * @return list of components
     */
    List<Component> getComponents();
    /**
     * Get Specific component in Board
     * @return list of components
     */
    List<Component> getSpecificComponent(Class classType );
    void displayState();
}
