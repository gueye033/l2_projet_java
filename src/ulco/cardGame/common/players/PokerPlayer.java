package ulco.cardGame.common.players;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PokerPlayer extends BoardPlayer{
    List<Card> cards;
    List<Coin> coins;
    public PokerPlayer(String name) {
        super(name);
        this.cards=new ArrayList<>();
        this.coins=new ArrayList<>();
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    @Override
    public Component play()  {
        Coin cardToPlay = (Coin)coins.get(0);
        // Remove card from player hand and update score
        this.removeComponent(cardToPlay);

        return cardToPlay;
    }

    @Override
    public void addComponent(Component component) {
        // On teste le type du `component` passé en paramètre
        // Si c'est une carte, on ajoute dans la liste des cartes
        if (component instanceof Card)
            cards.add((Card)component);
        // Si c'est un jeton, on ajoute dans la liste des jetons
        // Et on augmente le score du joueur
        if (component instanceof Coin) {
            coins.add((Coin) component);
            this.score += component.getValue();
        }
    }

    @Override
    public void removeComponent(Component component) {

        // Remove card from hand
        if (component instanceof Card){
            cards.remove(component);
           // update current player score (cards in hand)
            this.score = cards.size();
        }
        if (component instanceof Coin){
            coins.remove(component);
            // update current player score (coins in hand)
            this.score = coins.size();
        }
    }

    @Override
    public List<Component> getComponents() {
        List<Component> tmp = new ArrayList<>();
        tmp.addAll(cards);
        tmp.addAll(coins);
        return tmp;

        }

    @Override
    public void shuffleHand() {
        // prepare to shuffle hand
        Collections.shuffle(cards);
        Collections.shuffle(coins);
    }

    @Override
    public void clearHand() {

        // by default clear player hand
        // unlink each card
        for (Component card : cards) {
            card.setPlayer(null);
        }
        this.cards = new ArrayList<>();

        for (Component coin : coins) {
            coin.setPlayer(null);
        }
        this.coins = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "PokerPlayer{" +
                "cards=" + cards +
                ", coins=" + coins +
                ", name='" + name + '\'' +
                ", playing=" + playing +
                ", score=" + score +
                '}';
    }
}
